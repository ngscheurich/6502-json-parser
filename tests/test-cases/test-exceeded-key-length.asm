﻿
; @access public
; @return void
testExceededKeyLength .proc
	setJsonObject jsonObjectExceedKeyLength ; set JSON object from data.asm file
	expectJsonKey "first_96_bytes_of_this_very_very_very_long_key_are_going_to_be_parsed_and_the_rest_will_be_just_", longKeyIterator
	assertMemoryEqual longKeyString, longKeyTable, 3, "test exceeded key: long key #1 string"
	
	; Clean table, and try again with longer key value. As you see, still only the first 96 bytes are going to be matched.
	; You can increase the value of JSON_KEY_MAX_LENGTH constant up to 255 characters
	ldy #14
-
	lda longKeyEmptyTable,y
	sta longKeyTable,y
	dey
	bpl -
	
	setJsonObject jsonObjectExceedKeyLength
	expectJsonKey "first_96_bytes_of_this_very_very_very_long_key_are_going_to_be_parsed_and_the_rest_will_be_just_ignored", longKeyIterator
	assertMemoryEqual longKeyEmptyTable, longKeyTable, 15, "test exceeded key: long key #2 string"
rts


; @access private
; @return void
longKeyIterator
	storeJsonValue longKeyTableLocation
rts


longKeyTable
	.fill 15, 0
	
	
longKeyTableLocation
	.word longKeyTable
	

.enc "ascii"
.cdef 32, 126, 32

longKeyString
	.text "let me think..."

	
longKeyEmptyTable
	.fill 15, 0

.enc "none"
	
.pend
