﻿
; This is a special example to demonstrate the importance of calling nested iterators properly.
; Even if we're not going to do anything with date object in this example, if there is a call to dateIterator with this method:
; 
; expectJsonKey "date", dateIterator
; 
; then JSON Parser will enter this element.
; As parsing will begin in this nested element, data needs to be processed with expectJsonValue, or other function.
; In other words, if you're not going to process data of this element, just don't enter it...
; Otherwise parsing will produce unexpected side-effects. Uncomment expectJsonKey function in testExpectedElementExample to find it out.
;
; This test also tests empty objects, empty arrays and empty strings 
; 
; @access public
; @return void
testEmptyElements .proc
	setJsonObject jsonObjectExpectedElementExample ; set JSON object from data.asm file
	
-
	expectJsonKey	"colours", coloursIterator
	;expectJsonKey "date", dateIterator ; intentionally commented out, please uncomment to see unexpected behaviour
	expectJsonKey "empty_array", emptyArrayIterator
	expectJsonKey "empty_object", emptyObjectIterator
	expectJsonKey "empty_string", emptyStringIterator
	
	isJsonObjectCompleted
	beq +
		jmp -
+

	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "test empty elements: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "test empty elements: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 10, "test empty elements: light blue string"
	assertEqual #0, emptyArrayTable, "test empty elements: empty array"
	assertEqual #0, emptyObjectTable, "test empty elements: empty object"
	assertEqual #0, emptyStringTable, "test empty elements: empty string"
rts


; @access private
; @return void
dateIterator
	; Even if we're not going to do anything here with the value, dateIterator method has been already called,
	; so parser is nested into this element and you have to store values available inside using storeJsonValue, or iterate further
rts


; @access private
; @return void
emptyArrayIterator
	storeJsonValue emptyArrayLocation
rts


; @access private
; @return void
emptyObjectIterator
	storeJsonValue emptyObjectLocation
rts


; @access private
; @return void
emptyStringIterator
	storeJsonValue emptyStringLocation
rts


; @access private
; @return void
coloursIterator
-
	storeJsonValue coloursTableLocation, 16
	isJsonArrayCompleted
	bne -
rts


coloursTable
	.fill 16 * 3, 0
	

emptyArrayTable
	.byte 0
	
	
emptyObjectTable
	.byte 0
	

emptyStringTable
	.byte 0
	
	
coloursTableLocation
	.word coloursTable
	
	
emptyArrayLocation
	.word emptyArrayTable
	
	
emptyObjectLocation
	.word emptyObjectTable
	
	
emptyStringLocation
	.word emptyStringTable
	

.enc "ascii"
.cdef 32, 126, 32

purpleString
	.text "purple"

	
cyanString
	.text "cyan"

	
lightBlueString
	.text "light blue"
	

.enc "none"
	
.pend
