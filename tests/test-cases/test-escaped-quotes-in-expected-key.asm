﻿
; @access public
; @return void
testEscapedQuotesInExpectedKey .proc
	setJsonObject jsonObjectEscapedQuotesInKey ; set JSON object from data.asm file
-
	expectJsonKey '"light" blue', coloursIterator ; this one contains quote characters in key
	expectJsonKey "purple", coloursIterator
	expectJsonKey "cyan", coloursIterator
	isJsonObjectCompleted
	bne -

	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "esc quotes in exp key: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "esc quotes in exp key: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 12, "esc quotes in exp key: l. blue string"
	assertMemoryEqual purpleInteger, colourValuesTable + 1 * 0, 1, "esc quotes in exp key: purple int"
	assertMemoryEqual cyanInteger, colourValuesTable + 1 * 1, 1, "esc quotes in exp key: cyan int"
	assertMemoryEqual lightBlueInteger, colourValuesTable + 1 * 2, 2, "esc quotes in exp key: light blue int"
rts


; @access private
; @return void
coloursIterator
	storeJsonKey coloursTableLocation, 16
	storeJsonValue colourValuesTableLocation, 1
rts
	

coloursTable
	.fill 16 * 3, 0
	
	
colourValuesTable
	.fill 1 * 3, 0
	
	
coloursTableLocation
	.word coloursTable
	
	
colourValuesTableLocation
	.word colourValuesTable
	

.enc "ascii"
.cdef 32, 126, 32

	
purpleString
	.text "purple"

	
cyanString
	.text "cyan"

	
lightBlueString
	.text '"light" blue'
	
	
purpleInteger
	.text '5'

	
cyanInteger
	.text '4'
	

lightBlueInteger
	.text '14'
	
.enc "none"
	
.pend
