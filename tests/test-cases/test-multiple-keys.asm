﻿
; @access public
; @return void
testMultipleKeys .proc
	setJsonObject jsonObjectMultipleKeys ; set JSON object from data.asm file
	
-
	expectJsonKey "date", dateIterator
	expectJsonKey "shapes", shapesIterator
	expectJsonKey "colours", coloursIterator
	expectJsonKey "position", positionIterator
	isJsonObjectCompleted
	bne -

	assertMemoryEqual dateString, dateTable, 10, "test multiple keys: date string"
	assertMemoryEqual cubeString, shapesNameTable + 16 * 0, 4, "test multiple keys: cube string"
	assertMemoryEqual sphereString, shapesNameTable + 16 * 1, 6, "test multiple keys: sphere string"
	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "test multiple keys: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "test multiple keys: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 10, "test multiple keys: light blue string"
	assertMemoryEqual positionXString, positionXTable, 2, "test multiple keys: position x string"
	assertMemoryEqual positionYString, positionYTable, 2, "test multiple keys: position y string"
	assertMemoryEqual positionZString, positionZTable, 4, "test multiple keys: position z string"
rts


; @access private
; @return void
dateIterator
	storeJsonValue dateTableLocation
rts


; @access private
; @return void
coloursIterator
-
	storeJsonValue coloursTableLocation, 16
	isJsonArrayCompleted
	bne -
rts


; @access private
; @return void
shapesIterator
-
	expectJsonKey "name", setSingleShapeName
	expectJsonKey "height", setSingleShapeHeight
	isJsonObjectCompleted
	bne -
	
	isJsonArrayCompleted
	bne -
rts


; @access private
; @return void
positionIterator
-
	expectJsonKey "x", setX
	expectJsonKey "z", setZ
	expectJsonKey "y", setY
	isJsonObjectCompleted
	bne -
rts


; @access private
; @return void
setSingleShapeName
	storeJsonValue shapesNameTableLocation, 16
rts


; @access private
; @return void
setSingleShapeWidth
	storeJsonValue shapesWidthTableLocation, 16
rts


; @access private
; @return void
setSingleShapeHeight
	storeJsonValue shapesHeightTableLocation, 16
rts


; @access private
; @return void
setX
	storeJsonValue positionXTableLocation
rts


; @access private
; @return void
setY
	storeJsonValue positionYTableLocation
rts


; @access private
; @return void
setZ
	storeJsonValue positionZTableLocation
rts


dateTable
	.fill 10, 0


coloursTable
	.fill 16 * 3, 0
	

shapesNameTable
	.fill 16 * 5, 0
	
	
shapesWidthTable
	.fill 16 * 5, 0
	
	
shapesHeightTable
	.fill 16 * 5, 0

	
positionXTable
	.fill 8, 0

	
positionYTable
	.fill 8, 0

	
positionZTable
	.fill 8, 0
	

dateTableLocation
	.word dateTable

	
coloursTableLocation
	.word coloursTable
	
	
shapesNameTableLocation
	.word shapesNameTable
	
	
shapesWidthTableLocation
	.word shapesWidthTable
	
	
shapesHeightTableLocation
	.word shapesHeightTable

	
positionXTableLocation
	.word positionXTable
	

positionYTableLocation
	.word positionYTable

	
positionZTableLocation
	.word positionZTable
	
	
.enc "ascii"
.cdef 32, 126, 32

dateString
	.text "2017-08-13"
	
	
cubeString
	.text "cube"


sphereString
	.text "sphere"


purpleString
	.text "purple"

	
cyanString
	.text "cyan"

	
lightBlueString
	.text "light blue"
	
	
positionXString
	.text "16"
	
	
positionYString
	.text "25"
	
	
positionZString
	.text "1000"
	

.enc "none"
	
.pend
