﻿
; @access public
; @return void
testKeyValuePair .proc
	setJsonObject jsonObjectKeyValuePair ; set JSON object from data.asm file
	
-
	expectJsonKey	"some_non_existing_key", unusedKeyIterator
	expectJsonKey "date", dateIterator
	expectJsonKey	"some_other_non_existing_key", unusedKeyIterator

	isJsonObjectCompleted
	bne -

	assertMemoryEqual dateString, dateTable, 10, "test key-value pair: date string"
rts


; @access private
; @return void
dateIterator
	storeJsonValue dateTableLocation
rts


; @access private
; @return void
unusedKeyIterator
	; intentionally left blank
rts


dateTable
	.fill 10, 0
	
	
dateTableLocation
	.word dateTable
	

.enc "ascii"
.cdef 32, 126, 32

dateString
	.text "2017-08-13"
	

.enc "none"
	
.pend
