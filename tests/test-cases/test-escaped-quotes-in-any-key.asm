﻿
; @access public
; @return void
testEscapedQuotesInAnyKey .proc
	setJsonObject jsonObjectEscapedQuotesInKey ; set JSON object from data.asm file

-
	expectAnyJsonKey coloursIterator
	isJsonObjectCompleted
	bne -

	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "test esc quotes in key: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "test esc quotes in key: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 12, "test esc quotes in key: l. blue string"
	
	assertMemoryEqual purpleInteger, colourValuesTable + 2 * 0, 1, "test esc quotes in key: purple int"
	assertMemoryEqual cyanInteger, colourValuesTable + 2 * 1, 1, "test esc quotes in key: cyan int"
	assertMemoryEqual lightBlueInteger, colourValuesTable + 2 * 2, 2, "test esc quotes in key: light blue int"
	
	assertEqual #6, keyLengthTable + 0, "test esc quotes in key: purple length"
	assertEqual #4, keyLengthTable + 1, "test esc quotes in key: cyan length"
	assertEqual #12, keyLengthTable + 2, "test esc quotes in key: l. blue length"
rts


; @access private
; @return void
coloursIterator
	storeJsonKey coloursTableLocation, 16
	storeJsonKeyLength keyLengthTableLocation, 1
	storeJsonValue colourValuesTableLocation, 2
rts
	

coloursTable
	.fill 16 * 3, 0
	
	
colourValuesTable
	.fill 2 * 3, 0
	
	
keyLengthTable
	.fill 1 * 3, 0
	
	
coloursTableLocation
	.word coloursTable
	
	
colourValuesTableLocation
	.word colourValuesTable
	

keyLengthTableLocation
	.word keyLengthTable
	
	
.enc "ascii"
.cdef 32, 126, 32

	
purpleString
	.text "purple"

	
cyanString
	.text "cyan"

	
lightBlueString
	.text '"light" blue'
	
	
purpleInteger
	.text '5'

	
cyanInteger
	.text '4'
	

lightBlueInteger
	.text '14'
	
.enc "none"
	
.pend
