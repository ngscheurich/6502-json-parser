﻿
; @access public
; @return void
testStreamedObject2 .proc
	jsr setStream
	setStreamedJsonObject
	
	jsr readFromStream
-
	expectJsonKey "player", playerIterator
	expectJsonKey "value", valueIterator
	isJsonObjectCompleted
	bne -
	isJsonArrayCompleted
	bne -
	
	assertMemoryEqual playerString1, playerTable, 7, "test streamed object 2: player1 string"
	assertMemoryEqual valueString1, valueTable, 5, "test streamed object 2: value1 string"
	assertMemoryEqual playerString2, playerTable + 10, 5, "test streamed object 2: player2 string"
	assertMemoryEqual valueString2, valueTable + 10, 5, "test streamed object 2: value2 string"
	assertWordEqual 7, playerNameLengthTable, "test streamed object 2: player1 length"
	assertWordEqual 5, playerNameLengthTable + 10, "test streamed object 2: player2 length"
	assertWordEqual 5, valueLengthTable, "test streamed object 2: value1 length"
	assertWordEqual 5, valueLengthTable + 10, "test streamed object 2: value2 length"
rts


; Handler function to imitate the stream
; 
; @access private
; @return void
setStream
	lda #0
	sta bufferPosition
	
	; Set pointers to JSON object from data.asm file
	lda #<jsonObjectHiScores
	sta streamSource
	lda #>jsonObjectHiScores
	sta streamSource+1
	
	lda #<buffer
	sta bufferSource
	lda #>buffer
	sta bufferSource+1
rts


; @access private
; @return void
playerIterator
	storeJsonValue playerTableLocation, 10
	storeJsonValueLength playerNameLengthLocation, 10
rts


; @access private
; @return void
valueIterator
	storeJsonValue valueTableLocation, 10
	storeJsonValueLength valueLengthLocation, 10
rts


playerTable
	.fill 10 * 2, 0


valueTable
	.fill 10 * 2, 0


playerNameLengthTable
	.fill 10 * 2, 0
	
	
valueLengthTable
	.fill 10 * 2, 0
	
	
playerTableLocation
	.word playerTable

	
valueTableLocation
	.word valueTable
	
	
playerNameLengthLocation
	.word playerNameLengthTable
	
	
valueLengthLocation
	.word valueLengthTable
	
	
.enc "ascii"
.cdef 32, 126, 32

playerString1
	.text "gribbly"
	
	
playerString2
	.text "giana"
	
	
valueString1
	.text "65172"
	
	
valueString2
	.text "45387"

.enc "none"
	
.pend
