
; Variables
bufferPosition = $fa ; byte
streamSource = $fb ; word
bufferSource = $fd ; word

; Constants
BUFFER_SIZE = 10 ; how many bytes to keep in buffer


; Load current byte to accumulator
; Note: If Y register is used in this method, you have to save it, as JSON Parser uses it as well
; 
; @override
; @return A
JsonParser.getByte
	ldx bufferPosition
	lda buffer,x
rts


; Read next 10 bytes
; Note: If Y register is used in this method, you have to save it, as JSON Parser uses it as well
; 
; @override
; @return void
JsonParser.walkForward
	inc bufferPosition
	lda bufferPosition
	cmp #BUFFER_SIZE
	bne +
		lda #0
		sta bufferPosition

		; Simulate requesting the next #BUFFER_SIZE bytes from stream
		clc
		lda streamSource
		adc #<BUFFER_SIZE
		sta streamSource
		lda streamSource+1
		adc #>BUFFER_SIZE
		sta streamSource+1

		tya
		pha
		jsr readFromStream
		pla
		tay
+
rts


; Simulate reading from stream
; 
; @access public
; @return void
readFromStream
	ldy #0
-
	lda (streamSource),y
	sta (bufferSource),y
	iny
	cpy #BUFFER_SIZE
	bne -
rts


buffer
	.fill BUFFER_SIZE, 0
