﻿
; @access public
; @return void
testStreamedObject .proc
	jsr setStream
	setStreamedJsonObject
	
	jsr readFromStream
-
	expectJsonKey "date", dateIterator
	expectJsonKey "shapes", shapesIterator
	expectJsonKey "colours", coloursIterator
	expectJsonKey "position", positionIterator
	isJsonObjectCompleted
	bne -

	assertMemoryEqual dateString, dateTable, 10, "test streamed object: date string"
	assertMemoryEqual cubeString, shapesNameTable + 16 * 0, 4, "test streamed object: cube string"
	assertMemoryEqual sphereString, shapesNameTable + 16 * 1, 6, "test streamed object: sphere string"
	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "test streamed object: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "test streamed object: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 10, "test streamed object: light blue string"
	assertMemoryEqual positionXString, positionXTable, 2, "test streamed object: position x string"
	assertMemoryEqual positionYString, positionYTable, 2, "test streamed object: position y string"
	assertMemoryEqual positionZString, positionZTable, 4, "test streamed object: position z string"
rts


; Handler function to imitate the stream
; 
; @access private
; @return void
setStream
	lda #0
	sta bufferPosition
	
	; Set pointers to JSON object from data.asm file
	lda #<jsonObjectMultipleKeys
	sta streamSource
	lda #>jsonObjectMultipleKeys
	sta streamSource+1
	
	lda #<buffer
	sta bufferSource
	lda #>buffer
	sta bufferSource+1
rts


; @access private
; @return void
dateIterator
	storeJsonValue dateTableLocation
rts


; @access private
; @return void
coloursIterator
-
	storeJsonValue coloursTableLocation, 16
	isJsonArrayCompleted
	bne -
rts


; @access private
; @return void
shapesIterator
-
	expectJsonKey "name", setSingleShapeName
	expectJsonKey "height", setSingleShapeHeight
	isJsonObjectCompleted
	bne -
	
	isJsonArrayCompleted
	bne -
rts


; @access private
; @return void
positionIterator
-
	expectJsonKey "x", setX
	expectJsonKey "z", setZ
	expectJsonKey "y", setY
	isJsonObjectCompleted
	bne -
rts


; @access private
; @return void
setSingleShapeName
	storeJsonValue shapesNameTableLocation, 16
rts


; @access private
; @return void
setSingleShapeWidth
	storeJsonValue shapesWidthTableLocation, 16
rts


; @access private
; @return void
setSingleShapeHeight
	storeJsonValue shapesHeightTableLocation, 16
rts


; @access private
; @return void
setX
	storeJsonValue positionXTableLocation
rts


; @access private
; @return void
setY
	storeJsonValue positionYTableLocation
rts


; @access private
; @return void
setZ
	storeJsonValue positionZTableLocation
rts


dateTable
	.fill 10, 0


coloursTable
	.fill 16 * 3, 0
	

shapesNameTable
	.fill 16 * 5, 0
	
	
shapesWidthTable
	.fill 16 * 5, 0
	
	
shapesHeightTable
	.fill 16 * 5, 0

	
positionXTable
	.fill 8, 0

	
positionYTable
	.fill 8, 0

	
positionZTable
	.fill 8, 0
	

dateTableLocation
	.word dateTable

	
coloursTableLocation
	.word coloursTable
	
	
shapesNameTableLocation
	.word shapesNameTable
	
	
shapesWidthTableLocation
	.word shapesWidthTable
	
	
shapesHeightTableLocation
	.word shapesHeightTable

	
positionXTableLocation
	.word positionXTable
	

positionYTableLocation
	.word positionYTable

	
positionZTableLocation
	.word positionZTable
	
	
.enc "ascii"
.cdef 32, 126, 32

dateString
	.text "2017-08-13"
	
	
cubeString
	.text "cube"


sphereString
	.text "sphere"


purpleString
	.text "purple"

	
cyanString
	.text "cyan"

	
lightBlueString
	.text "light blue"
	
	
positionXString
	.text "16"
	
	
positionYString
	.text "25"
	
	
positionZString
	.text "1000"
	

.enc "none"
	
.pend
