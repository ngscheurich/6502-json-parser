﻿
; @access public
; @return void
testSetJsonObject
	setJsonObject jsonObjectKeyValuePair ; set JSON object from data.asm file
	
	assertWordEqual jsonObjectKeyValuePair, JsonParser.jsonObjectMemoryLocation, "test set json object"
rts
