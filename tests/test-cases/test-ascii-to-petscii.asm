﻿
; @access public
; @return void
testAsciiToPetscii .proc
	setJsonObject jsonObjectAsciiToPetscii ; set JSON object from data.asm file
	setPetsciiMode
	
-
	expectJsonKey "alphabet_lowercase", alphabetLowercaseIterator
	expectJsonKey "alphabet_uppercase", alphabetUppercaseIterator
	expectJsonKey "integers", integersIterator
	expectJsonKey "special_characters", specialCharactersIterator
	isJsonObjectCompleted
	beq +
		jmp -
+

	assertMemoryEqual alphabetLowercaseString, alphabetLowercaseTable, size(alphabetLowercaseString), "test ascii - petscii: alphabet lowercase"
	assertMemoryEqual alphabetUppercaseString, alphabetUppercaseTable , size(alphabetUppercaseString), "test ascii - petscii: alphabet uppercase"
	assertMemoryEqual integersString, integersTable, size(integersString), "test ascii - petscii: integers"
	assertMemoryEqual specialCharactersString, specialCharactersTable, specialCharactersStringEnd - specialCharactersString, "test ascii - petscii: special characters"
rts


; @access private
; @return void
alphabetLowercaseIterator
	storeJsonValue alphabetLowercaseTableLocation
rts


; @access private
; @return void
alphabetUppercaseIterator
	storeJsonValue alphabetUppercaseTableLocation
rts


; @access private
; @return void
integersIterator
	storeJsonValue integersTableLocation
rts


; @access private
; @return void
specialCharactersIterator
	storeJsonValue specialCharactersTableLocation
rts


alphabetLowercaseTable
	.fill 40, 0
	
	
alphabetUppercaseTable
	.fill 40, 0


integersTable
	.fill 40, 0


specialCharactersTable
	.fill 40, 0


alphabetLowercaseTableLocation
	.word alphabetLowercaseTable
	
	
alphabetUppercaseTableLocation
	.word alphabetUppercaseTable
	
	
integersTableLocation
	.word integersTable
	
	
specialCharactersTableLocation
	.word specialCharactersTable
	

.enc "screen"

alphabetLowercaseString .text "abcdefghijklmnopqrstuvwxyz"
	
	
alphabetUppercaseString .text "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


integersString .text "0123456789"


specialCharactersString
	.text '!@"#$%'
	.byte 30 ; up-arrow character
	.text '&*()[]<>/?'
	.byte 100 ; underscore character
	.text '=+-:;,.'
	.byte 39 ; apostrophe character

specialCharactersStringEnd

.enc "none"

	
.pend
