﻿
; @access public
; @return void
testArrayOfStrings .proc
	setJsonObject jsonObjectArrayOfStrings ; set JSON object from data.asm file
	setJsonOutputMemoryLocation coloursTableLocation, coloursTable
	setJsonOutputMemoryLocation lengthTableLocation, lengthTable
	
	jsr coloursIterator

	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "test array of strings: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "test array of strings: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 10, "test array of strings: light blue string"
	assertWordEqual 6, lengthTable + 2 * 0, "test array of strings: purple length"
	assertWordEqual 4, lengthTable + 2 * 1, "test array of strings: cyan length"
	assertWordEqual 10, lengthTable + 2 * 2, "test array of strings: light blue length"
rts


; @access private
; @return void
coloursIterator
-
	storeJsonValue coloursTableLocation, 16
	storeJsonValueLength lengthTableLocation, 2
	isJsonArrayCompleted
	bne -
rts


coloursTable
	.fill 16 * 3, 0
	
	
lengthTable
	.fill 2 * 3, 0
	

coloursTableLocation
	.word 0
	
	
lengthTableLocation
	.word 0
	

.enc "ascii"
.cdef 32, 126, 32

purpleString
	.text "purple"

	
cyanString
	.text "cyan"

	
lightBlueString
	.text "light blue"
	

.enc "none"
	
.pend
