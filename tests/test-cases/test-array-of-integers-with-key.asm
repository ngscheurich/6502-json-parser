﻿
; @access public
; @return void
testArrayOfIntegersWithKey .proc
	setJsonObject jsonObjectArrayOfIntegersWithKey ; set JSON object from data.asm file
	setJsonOutputMemoryLocation numbersTableLocation, numbersTable
	
	expectJsonKey "numbers", numbersIterator
	
	assertMemoryEqual firstNumberString, numbersTable + 8 * 0, 5, "test array of integers with key: #1"
	assertMemoryEqual secondNumberString, numbersTable + 8 * 1, 2, "test array of integers with key: #2"
	assertMemoryEqual thirdNumberString, numbersTable + 8 * 2, 4, "test array of integers with key: #3"
rts


; @access private
; @return void
numbersIterator
-
	storeJsonValue numbersTableLocation, 8
	isJsonArrayCompleted
	bne -
rts


numbersTable
	.fill 8 * 3, 0
	
	
numbersTableLocation
	.word 0
	

.enc "ascii"
.cdef 32, 126, 32

firstNumberString
	.text "16.05"

	
secondNumberString
	.text "64"

	
thirdNumberString
	.text "1024"
	

.enc "none"
	
.pend
