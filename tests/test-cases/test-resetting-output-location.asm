﻿
; @access public
; @return void
testResettingOutputLocation .proc
	setJsonObject jsonObjectArrayOfStringsWithKey ; set JSON object from data.asm file
	setJsonOutputMemoryLocation coloursTableLocation, coloursTable
	
-
	expectJsonKey "colours", coloursIterator
	isJsonObjectCompleted
	bne -

	; Assert
	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "test array of strings w/ key: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "test array of strings w/ key: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 10, "test array of strings w/ key: l. blue string"
	
	; Repeat again
	jsr clearOutputMemoryLocation
	setJsonObject jsonObjectArrayOfStringsWithKey ; set JSON object from data.asm file
	setJsonOutputMemoryLocation coloursTableLocation, coloursTable
-
	expectJsonKey "colours", coloursIterator
	isJsonObjectCompleted
	bne -
	
	; Assert
	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "reset output location: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "reset output location: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 10, "reset output location: l. blue string"
	
	; Repeat again
	jsr clearOutputMemoryLocation
	setJsonObject jsonObjectArrayOfStringsWithKey ; set JSON object from data.asm file
	setJsonOutputMemoryLocation coloursTableLocation, coloursTable
-
	expectJsonKey "colours", coloursIterator
	isJsonObjectCompleted
	bne -
	
	; Assert
	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "test array of strings w/ key: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "test array of strings w/ key: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 10, "test array of strings w/ key: l. blue string"
rts


; @access private
; @return void
coloursIterator
-
	storeJsonValue coloursTableLocation, 16
	isJsonArrayCompleted
	bne -
rts


; Internal method to clean output memory location for the next run
;
; @access private
; @return void
clearOutputMemoryLocation
	lda #<coloursTable
	sta clearPointer+1
	lda #>coloursTable
	sta clearPointer+2
	
	ldy #16 * 3 - 1
	lda #0
clearPointer
	sta $1234,y
	dey
	bpl clearPointer
rts


coloursTable
	.fill 16 * 3, 0
	
	
coloursTableLocation
	.word 0
	

.enc "ascii"
.cdef 32, 126, 32

purpleString
	.text "purple"

	
cyanString
	.text "cyan"

	
lightBlueString
	.text "light blue"
	

.enc "none"
	
.pend
