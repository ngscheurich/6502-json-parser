﻿
; @access public
; @return void
testArrayOfStringsWithKey .proc
	setJsonObject jsonObjectArrayOfStringsWithKey ; set JSON object from data.asm file
	setJsonOutputMemoryLocation coloursTableLocation, coloursTable

-
	expectJsonKey "some_non_existing_key", unusedKeyIterator
	expectJsonKey "colours", coloursIterator
	expectJsonKey "some_other_non_existing_key", unusedKeyIterator

	isJsonObjectCompleted
	bne -

	assertMemoryEqual purpleString, coloursTable + 16 * 0, 6, "array of strings w/ key: purple string"
	assertMemoryEqual cyanString, coloursTable + 16 * 1, 4, "array of strings w/ key: cyan string"
	assertMemoryEqual lightBlueString, coloursTable + 16 * 2, 10, "array of strings w/ key: l. blue string"
rts


; @access private
; @return void
coloursIterator	
-
	storeJsonValue coloursTableLocation, 16
	isJsonArrayCompleted
	bne -
rts


; @access private
; @return void
unusedKeyIterator
	; intentionally left blank
rts


coloursTable
	.fill 16 * 3, 0
	
	
coloursTableLocation
	.word 0
	

.enc "ascii"
.cdef 32, 126, 32

purpleString
	.text "purple"

	
cyanString
	.text "cyan"

	
lightBlueString
	.text "light blue"
	

.enc "none"
	
.pend
