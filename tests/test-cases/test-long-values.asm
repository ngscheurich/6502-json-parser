﻿
; @access public
; @return void
testLongValues .proc
	setJsonObject jsonObjectLongValues ; set JSON object from data.asm file
	
-
	expectJsonKey "colours", coloursIterator

	isJsonObjectCompleted
	bne -
	
	assertMemoryEqual coloursString, coloursTable, coloursStringEnd - coloursString, "test long values: colours string"
	assertWordEqual coloursStringEnd - coloursString, lengthTable, "test long values: length"
rts


; @access private
; @return void
coloursIterator
	storeJsonValue coloursTableLocation
	storeJsonValueLength lengthTableLocation
rts


coloursTable
	.fill coloursStringEnd - coloursString, 0
	
	
lengthTable
	.word 0
	
	
coloursTableLocation
	.word coloursTable
	
	
lengthTableLocation
	.word lengthTable

	
.enc "ascii"
.cdef 32, 126, 32


coloursString
	.for i=0, i<10, i=i+1
		.text 'black, white, red, cyan, purple, green, blue, yellow, '
		.text 'orange, brown, pink, dark gray, medium gray, light green, light blue, light gray, '
	.next
	
coloursStringEnd
	

.enc "none"
	
.pend
