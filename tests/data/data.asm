﻿
.enc "ascii"
.cdef 32, 126, 32
	
	
jsonObjectExceedKeyLength
	.text '{"first_96_bytes_of_this_very_very_very_long_key_are_going_to_be_parsed_and_the_rest_will_be_just_ignored":"let me think..."}'

	
jsonObjectKeyValuePair
	.text '{"date":"2017-08-13"}'
	
	
jsonObjectArrayOfStrings
	.text '["purple","cyan","light blue"]'
	
	
jsonObjectArrayOfStringsWithKey
	.text '{"colours":["purple","cyan","light blue"]}'
	
	
jsonObjectArrayOfIntegers
	.text '[16.05,64,1024]'
	
	
jsonObjectArrayOfIntegersWithKey
	.text '{"numbers":[16.05,64,1024]}'
	
	
jsonObjectMultipleKeys
	.text '{"date":"2017-08-13",'
	.text '"colours":["purple","cyan","light blue"],'
	.text '"shapes":[{"name":"cube","width":178,"height":178,"length":178},{"name":"sphere","width":50,"height":150,"length":50}],'
	.text '"position":{"x":16,"y":25,"z":1000}}'
	
	
jsonObjectExpectedElementExample
	.text '{"date":"2017-08-13","colours":["purple","cyan","light blue"],"empty_array":[],"empty_object":{},"empty_string":""}'
	
	
jsonObjectEscapedQuotes
	.text '{"i_just_wanted_to_say":"someone put \"me\" in quotes?!","colours":["purple","cyan","\"light\" blue"]}'
	
	
jsonObjectEscapedQuotesInKey
	.text '{"purple":5,"cyan":4,"\"light\" blue":14}'
	
	
jsonObjectAnyKeyObject
	.text '{"circus tom":49500,"jet set willy":43750,"slaine":38550}'


jsonObjectAsciiToPetscii
	.text '{'
	.text '"alphabet_lowercase":"abcdefghijklmnopqrstuvwxyz",'
	.text '"alphabet_uppercase":"ABCDEFGHIJKLMNOPQRSTUVWXYZ",'
	.text '"integers":"0123456789",'
	.text '"special_characters":"!@\"#$%^&*()[]<>\/?_=+-:;,.'
	.text "'" ; apostrophe character
	.text '"'
	.text '}'
	
	
jsonObjectLongValues
	.text '{'
	.text '"colours":"'
	.for i=0, i<10, i=i+1
		; length of this string is 1360 bytes in total
		.text 'black, white, red, cyan, purple, green, blue, yellow, '
		.text 'orange, brown, pink, dark gray, medium gray, light green, light blue, light gray, '
	.next
	.text '"}'
	
	
jsonObjectHiScores
	.text '[{"player":"gribbly","value":65172},{"player":"giana","value":45387}]'

.enc "none"
