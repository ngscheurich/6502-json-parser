﻿
	; Include c64unit
	.include "../vendor/c64unit/cross-assemblers/64tass/core2000.asm"
	
	; Init
	c64unit
	
	; Examine test cases
	examineTest testStreamedObject
	examineTest testStreamedObject2
	
	; If this point is reached, there were no assertion fails
	c64unitExit
	
	; Include domain logic, i.e. classes, methods and tables
	.include "../src/json-parser.asm"
	.include "data/data.asm"
	
	; Testsuite with all test cases
	.include "test-cases/test-streamed-object-init.asm"
	.include "test-cases/test-streamed-object.asm"
	.include "test-cases/test-streamed-object-2.asm"
