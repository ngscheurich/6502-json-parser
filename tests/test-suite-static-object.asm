﻿
	; Include c64unit
	.include "../vendor/c64unit/cross-assemblers/64tass/core5000.asm"
	
	; Init
	c64unit
	
	; Examine test cases
	examineTest testExceededKeyLength
	examineTest testSetJsonObject
	examineTest testKeyValuePair
	examineTest testArrayOfStrings
	examineTest testArrayOfStringsWithKey
	examineTest testArrayOfIntegers
	examineTest testArrayOfIntegersWithKey
	examineTest testMultipleKeys
	examineTest testEmptyElements
	examineTest testEscapedQuotes
	examineTest testEscapedQuotesInAnyKey
	examineTest testEscapedQuotesInExpectedKey
	examineTest testAsciiToPetscii
	examineTest testAnyKeyObject
	examineTest testLongValues
	examineTest testResettingOutputLocation
	
	; If this point is reached, there were no assertion fails
	c64unitExit
	
	; Include domain logic, i.e. classes, methods and tables
	.include "../src/json-parser.asm"
	.include "data/data.asm"
	
	; Testsuite with all test cases
	.include "test-cases/test-exceeded-key-length.asm"
	.include "test-cases/test-set-json-object.asm"
	.include "test-cases/test-key-value-pair.asm"
	.include "test-cases/test-array-of-strings.asm"
	.include "test-cases/test-array-of-strings-with-key.asm"
	.include "test-cases/test-array-of-integers.asm"
	.include "test-cases/test-array-of-integers-with-key.asm"
	.include "test-cases/test-multiple-keys.asm"
	.include "test-cases/test-empty-elements.asm"
	.include "test-cases/test-escaped-quotes.asm"
	.include "test-cases/test-escaped-quotes-in-any-key.asm"
	.include "test-cases/test-escaped-quotes-in-expected-key.asm"
	.include "test-cases/test-ascii-to-petscii.asm"
	.include "test-cases/test-any-key-object.asm"
	.include "test-cases/test-long-values.asm"
	.include "test-cases/test-resetting-output-location.asm"
	
