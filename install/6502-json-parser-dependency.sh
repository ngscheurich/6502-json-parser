#!/usr/bin/env bash
if [ -d vendor/6502-json-parser ];
then
    cd vendor/6502-json-parser
    git pull http://Commocore@bitbucket.org/Commocore/6502-json-parser.git master
    cd ../../
else
    git clone --origin 6502-json-parser http://Commocore@bitbucket.org/Commocore/6502-json-parser.git vendor/6502-json-parser
fi
