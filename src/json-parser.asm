﻿

JsonParser .proc

; @access public
; @return void
setAsciiMode
	lda #0
	sta storeJsonValue.asciiToPetsciiMode+1
rts


; @access public
; @return void
setPetsciiMode
	lda #1
	sta storeJsonValue.asciiToPetsciiMode+1
rts


; @access public
; @param X - lo-address byte location of key name
; @param Y - hi-address byte location of key name
; @param A - key name length
; @return void
setExpectededJsonKey
	stx executeSubroutine.expectedKey+1
	sty executeSubroutine.expectedKey+2
	sta executeSubroutine.keyLength+1
rts


; @access public
; @param X
; @param Y
; @noreturn passes to prepareParser
setJsonObject
	stx jsonObjectMemoryLocation
	sty jsonObjectMemoryLocation+1


; @access public
; @return void
prepareParser
	lda #1
	sta currentNestingLevel
	
	lda #0
	sta storeJsonValue.asciiToPetsciiMode+1
	sta jsonKeyLoaded
	sta jsonKeyBufferLength
	
	ldy #255
-
	sta nestingLevelAcknowledge-1,y
	dey
	bne -
rts


; @access public
; @param X - lo-address byte of pointer location
; @param Y - hi-address byte of pointer location
; @return void
setJsonOutputMemoryLocationPointer
	stx setJsonOutputMemoryLocation.pointerLo+1
	sty setJsonOutputMemoryLocation.pointerLo+2
	stx setJsonOutputMemoryLocation.pointerHi+1
	sty setJsonOutputMemoryLocation.pointerHi+2
rts


; @access public
; @param X - lo-address byte of output location
; @param Y - hi-address byte of output location
; @return void
setJsonOutputMemoryLocation .proc

pointerLo
	stx $1234
	
	tya
	ldx #1
pointerHi
	sta $1234,x
rts


.pend


; @access public
; @param X - lo-address byte location of method iterator
; @param Y - hi-address byte location of method iterator
; @return void
setMethodIterator
	stx executeSubroutine.advanceToMethodIterator+1
	sty executeSubroutine.advanceToMethodIterator+2
rts


; @access public
; @needs advanceToMethodIterator pointers to be set before
; @return void
expectAnyJsonKey
	lda #0
	sta jsonKeyBufferLength
	sta storeJsonKeyLength.length+1
	jsr lookForBufferKey
	jsr executeSubroutine.execute
rts


; @access public
; @needs advanceToMethodIterator pointers to be set before
; @return void
expectJsonKey
	lda jsonKeyLoaded
	bne +
		; Key haven't been put to buffer yet, look for the first occurrence and put into buffer
		jsr lookForBufferKey
+
	jsr executeSubroutine
rts


; @access public
; @param X - <outputMemoryPointer
; @param Y - >outputMemoryPointer
; @return void
storeJsonValue .proc
	stx pointer+1
	sty pointer+2
	
	ldy #1
pointer
	lda $1234,y
	sta output+1,y
	dey
	bpl pointer
	
	lda output+1
	sta storeJsonValueLength.beginLo+1
	lda output+2
	sta storeJsonValueLength.beginHi+1
	
	ldy #0
	sty stringValue
	sty jsonKeyBufferLength
		
	; Is it an array?
	jsr expectJsonArrayOrObject
	beq +
		jsr walkForward
+
	
	; Is it an empty array?
	jsr getByte
	cmp #RIGHT_SQUARE_BRACKET_CHAR
	bne +
		jsr walkForward
		rts
+
	;Is it an empty object?
	cmp #RIGHT_CURLY_BRACKET_CHAR
	bne +
		jsr walkForward
		rts
+
	; Is it a string value?
	cmp #QUOTE_CHAR
	bne ++
		ldx #1
		stx stringValue
		jsr walkForward
	
-
	jsr getByte
	jsr isEndOfValue
	bne ++
		ldx stringValue
		beq +
			jsr walkForward ; walk forward if it was a string value
+
		rts
+
	; Escaped character?
	cmp #BACKSLASH_CHAR
	bne +
		jsr walkForward
		jsr getByte
+

asciiToPetsciiMode
	ldx #0 ; value modified on the fly
	beq +
		jsr convertAsciiToPetscii
+
output
	sta $1234
	
	inc output+1
	bne +
		inc output+2
+
	
	jsr walkForward
	
	jmp -
rts


.pend


; @access public
; @param X - <outputMemoryPointer
; @param Y - >outputMemoryPointer
; @return void
storeJsonValueLength .proc
	stx pointer+1
	sty pointer+2
	
	ldy #1
pointer
	lda $1234,y
	sta output1+1,y
	sta output2+1,y
	dey
	bpl pointer

	sec
	ldy #0
	lda storeJsonValue.output+1
beginLo
	sbc #0
output1
	sta $1234,y
	
	iny
	lda storeJsonValue.output+2
beginHi
	sbc #0
output2
	sta $1234,y
rts

.pend


; @access private
; @param A - character
; @return zero flag
isEndOfValue
	ldx stringValue
	beq isEndOfIntegerValue
	

isEndOfStringValue
	cmp #QUOTE_CHAR
	beq +
		ldx #1 ; return non-zero flag status
		rts
+
	ldx #0 ; return zero flag status
rts

	
isEndOfIntegerValue
	cmp #RIGHT_CURLY_BRACKET_CHAR
	beq +
		cmp #RIGHT_SQUARE_BRACKET_CHAR
		beq +
			cmp #COMMA_CHAR
			beq +
				ldx #1 ; return non-zero flag status
				rts
+
	jsr checkIfEndOfJsonElement
	ldx #0 ; return zero flag status
rts


; @param A - character value
checkIfEndOfJsonElement
	cmp #RIGHT_CURLY_BRACKET_CHAR
	beq +
		cmp #RIGHT_SQUARE_BRACKET_CHAR
		beq +
			rts
+
	dec currentNestingLevel
rts


; @access private
; @return zero flag
expectJsonArrayOrObject
	jsr getByte
	cmp #LEFT_SQUARE_BRACKET_CHAR
	bne +
		ldx #1 ; return non-zero flag status
		rts
+
	cmp #LEFT_CURLY_BRACKET_CHAR
	bne +
		ldx #1 ; return non-zero flag status
		rts
+
	ldx #0 ; return zero flag status
rts


; @access private
; @return void
executeSubroutine .proc
	ldy #0
-
expectedKey
	lda $1234,y
	cmp jsonKeyBuffer,y
	
	bne +
		iny
keyLength
		cpy #0
		bne -
			ldy #0
			sty jsonKeyLoaded
execute
			; Both keys are equal
			
			; Acknowledge current nesting level
			ldy currentNestingLevel
			lda #1
			sta nestingLevelAcknowledge,y
			
			inc currentNestingLevel
			
advanceToMethodIterator
			jsr $1234
			
			dec currentNestingLevel
+
rts

.pend


; @access private
; @input A - input ASCII character
; @return A - output PETSCII character
convertAsciiToPetscii
	cmp #$61 ; convert to lowercase
	bcc +
		sec
		sbc #$60
		rts
+
	cmp #AT_CHAR
	bne +
		lda #AT_PETSCII_CHAR
		rts
+
	cmp #CARET_CHAR
	bne +
		lda #UP_ARROW_PETSCII_CHAR
		rts
+
	cmp #LEFT_SQUARE_BRACKET_CHAR
	bne +
		lda #LEFT_SQUARE_BRACKET_PETSCII_CHAR
		rts
+
	cmp #RIGHT_SQUARE_BRACKET_CHAR
	bne +
		lda #RIGHT_SQUARE_BRACKET_PETSCII_CHAR
		rts
+
	cmp #UNDERSCORE_CHAR
	bne +
		lda #UNDERSCORE_PETSCII_CHAR
		rts
+
rts


; @access private
; @return void
lookForBufferKey
	ldy #0
	sty bufferingMode
-
	jsr getByte
	cmp #RIGHT_CURLY_BRACKET_CHAR
	bne +
		rts
+

	; Escaped character? If yes then treat as regular character, not function one
	cmp #BACKSLASH_CHAR
	bne +
		jsr walkForward
		jsr getByte
		jmp ++
+
	cmp #QUOTE_CHAR
	bne +
		inc bufferingMode
		lda bufferingMode
		cmp #2
		bne ++
			jmp bufferKeyLoaded
+
	
	ldx bufferingMode
	beq +
		sta jsonKeyBuffer,y
		iny
		cpy #JSON_KEY_MAX_LENGTH
		bne +
			jsr walkForwardToTheEndOfKey
			jmp bufferKeyLoaded
+
	jsr walkForward
jmp -


; @access private
; @param Y - key buffer length
; @sets jsonKeyLoaded
; @return void
bufferKeyLoaded
	sty jsonKeyBufferLength
	sty storeJsonKeyLength.length+1
	ldy #1
	sty jsonKeyLoaded
	jsr walkForward
	jsr walkForward
rts


; @access public
; @param X - <outputMemoryPointer
; @param Y - >outputMemoryPointer
; @return void
storeJsonKeyFromBuffer .proc
	stx pointer+1
	sty pointer+2
	
	ldy #1
pointer
	lda $1234,y
	sta output+1,y
	dey
	bpl pointer

	ldy #0
-
	lda jsonKeyBuffer,y
output
	sta $1234,y
	iny
	cpy jsonKeyBufferLength
	bne -
	
	lda #0
	sta jsonKeyBufferLength
rts

.pend


; @access public
; @param X - <outputMemoryPointer
; @param Y - >outputMemoryPointer
; @return void
storeJsonKeyLength .proc
	stx pointer+1
	sty pointer+2
	
	ldy #1
pointer
	lda $1234,y
	sta output+1,y
	dey
	bpl pointer

length
	lda #0
	
output
	sta $1234
rts

.pend


; Adjust pointers for currently used table for the next key
; 
; @access public
; @param X - <size
; @param Y - >size
; @uses storeJsonKeyFromBuffer.pointer
; @return void
moveStoreJsonKeyPointerLocation .proc
	stx sizeLo+1
	sty sizeHi+1

	lda storeJsonKeyFromBuffer.pointer+1
	sta pointerInLo+1
	sta pointerOutLo+1
	sta pointerInHi+1
	sta pointerOutHi+1

	lda storeJsonKeyFromBuffer.pointer+2
	sta pointerInLo+2
	sta pointerOutLo+2
	sta pointerInHi+2
	sta pointerOutHi+2
	
	clc
	ldy #0
	
pointerInLo
	lda $1234,y
sizeLo
	adc #0
pointerOutLo
	sta $1234,y
	
	iny
pointerInHi
	lda $1234,y
sizeHi
	adc #0
pointerOutHi
	sta $1234,y

rts

.pend


; Adjust pointers for currently used length table for the next key
; 
; @access public
; @param X - <size
; @param Y - >size
; @uses storeJsonKeyLength.pointer
; @return void
moveStoreJsonKeyLengthPointerLocation .proc
	stx sizeLo+1
	sty sizeHi+1

	lda storeJsonKeyLength.pointer+1
	sta pointerInLo+1
	sta pointerOutLo+1
	sta pointerInHi+1
	sta pointerOutHi+1

	lda storeJsonKeyLength.pointer+2
	sta pointerInLo+2
	sta pointerOutLo+2
	sta pointerInHi+2
	sta pointerOutHi+2
	
	clc
	ldy #0
	
pointerInLo
	lda $1234,y
sizeLo
	adc #0
pointerOutLo
	sta $1234,y
	
	iny
pointerInHi
	lda $1234,y
sizeHi
	adc #0
pointerOutHi
	sta $1234,y

rts

.pend


; Adjust pointers for currently used table for the next value
; 
; @access public
; @param X - <size
; @param Y - >size
; @uses storeJsonValue.pointer
; @return void
moveStoreJsonValuePointerLocation .proc
	stx sizeLo+1
	sty sizeHi+1

	lda storeJsonValue.pointer+1
	sta pointerInLo+1
	sta pointerOutLo+1
	sta pointerInHi+1
	sta pointerOutHi+1

	lda storeJsonValue.pointer+2
	sta pointerInLo+2
	sta pointerOutLo+2
	sta pointerInHi+2
	sta pointerOutHi+2
	
	clc
	ldy #0
	
pointerInLo
	lda $1234,y
sizeLo
	adc #0
pointerOutLo
	sta $1234,y
	
	iny
pointerInHi
	lda $1234,y
sizeHi
	adc #0
pointerOutHi
	sta $1234,y

rts

.pend


; Adjust pointers for currently used length table for the next value
; 
; @access public
; @param X - <size
; @param Y - >size
; @uses storeJsonValueLength.pointer
; @return void
moveStoreJsonValueLengthPointerLocation .proc
	stx sizeLo+1
	sty sizeHi+1

	lda storeJsonValueLength.pointer+1
	sta pointerInLo+1
	sta pointerOutLo+1
	sta pointerInHi+1
	sta pointerOutHi+1

	lda storeJsonValueLength.pointer+2
	sta pointerInLo+2
	sta pointerOutLo+2
	sta pointerInHi+2
	sta pointerOutHi+2
	
	clc
	ldy #0
	
pointerInLo
	lda $1234,y
sizeLo
	adc #0
pointerOutLo
	sta $1234,y
	
	iny
pointerInHi
	lda $1234,y
sizeHi
	adc #0
pointerOutHi
	sta $1234,y

rts

.pend


; @access public
; @return zero flag
isJsonObjectCompleted
	lda currentNestingLevel
	cmp #0
	bne +
		jmp exitFromJsonObjectLoop
+

	jsr getByte
	cmp #RIGHT_CURLY_BRACKET_CHAR
	bne +
		jmp exitFromJsonObjectLoop
+

	; Key not found in procedures, let's ignore it and move to another JSON element
	ldy currentNestingLevel
	lda nestingLevelAcknowledge,y
	bne +
		jsr walkForwardToAnotherJsonElement
+

	; Clear acknowledge for current level
	lda #0
	ldy currentNestingLevel
	sta nestingLevelAcknowledge,y

	lda #1 ; return non-zero flag status
rts


; @access public
; @return zero flag
isJsonArrayCompleted
	jsr getByte
	cmp #RIGHT_SQUARE_BRACKET_CHAR
	bne +
		jmp exitFromJsonObjectLoop
+
	jsr walkForward
	lda #1 ; return non-zero flag status
rts


; @access private
; @affects jsonKeyBufferLength
; @affects jsonValueBufferLength
; @return zero flag
exitFromJsonObjectLoop
	jsr walkForward
	lda #0
	sta jsonKeyLoaded
	sta jsonKeyBufferLength
	lda #0 ; return zero flag status
rts


; @access private
; @return void
walkForwardToTheEndOfKey
-
	jsr walkForward
	jsr getByte
	cmp #QUOTE_CHAR
	bne +
		rts
+
	jmp -
rts


; @access private
; @return void
walkForwardToAnotherJsonElement .proc
	lda #0
	sta stringValue
	sta arrayNesting
	sta objectNesting
-
	jsr getByte
	ldx stringValue
	beq +
		; String mode
		cmp #QUOTE_CHAR
		bne loop
			ldx #0
			stx stringValue
			jmp loop
+
	; Non-string mode
	cmp #QUOTE_CHAR
	bne +
		ldx #1
		stx stringValue
		jmp loop
+
	cmp #LEFT_SQUARE_BRACKET_CHAR
	bne +
		inc arrayNesting
		jmp loop
+
	cmp #RIGHT_SQUARE_BRACKET_CHAR
	bne +
		dec arrayNesting
		jsr isCurrentJsonObjectClosing
		bne exit
		jmp loop
+
	cmp #LEFT_CURLY_BRACKET_CHAR
	bne +
		inc objectNesting
		jmp loop
+
	cmp #RIGHT_CURLY_BRACKET_CHAR
	bne +
		lda objectNesting
		beq exit
		dec objectNesting
		jsr isCurrentJsonObjectClosing
		bne exit
		jmp loop
+
	cmp #COMMA_CHAR
	bne +
		jmp exit
+
jmp loop


exit
	lda #0
	sta jsonKeyLoaded
	sta jsonKeyBufferLength
	rts


loop
	jsr walkForward
	jmp -
	

; @access private
; @return zero flag
isCurrentJsonObjectClosing
	lda objectNesting
	bne +
		lda arrayNesting
		bne +
			lda #1
			rts
+
	lda #0
rts


arrayNesting
	.byte 0


objectNesting
	.byte 0
	
	
.pend


.include "includes/constants.asm"
.include "includes/variables.asm"
.include "includes/tables.asm"
.include "includes/parse-static-object.asm"

.pend


.include "includes/macros.asm"
.include "includes/debug.asm"
