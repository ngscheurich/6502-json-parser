﻿
; @access public
; @param word methodIterator
expectAnyJsonKey .macro methodIterator
	ldx #<\methodIterator
	ldy #>\methodIterator
	jsr JsonParser.setMethodIterator

	jsr JsonParser.expectAnyJsonKey
.endm


; @access public
; @param string key
; @param word methodIterator
expectJsonKey .macro key, methodIterator
	; Trick to create text label dynamically
	jmp +
	.enc "ascii"
	.cdef 32, 126, 32
	
key .text \key
	.enc "none"
+
	lda #size(key)
	ldx #<key
	ldy #>key
	jsr JsonParser.setExpectededJsonKey
	
	ldx #<\methodIterator
	ldy #>\methodIterator
	jsr JsonParser.setMethodIterator
	
	jsr JsonParser.expectJsonKey
.endm


; @access public
; @return carry flag
isJsonObjectCompleted .macro
	jsr JsonParser.isJsonObjectCompleted
.endm


; @access public
; @return carry flag
isJsonArrayCompleted .macro
	jsr JsonParser.isJsonArrayCompleted
.endm


; @access public
; @return void
setAsciiMode .macro
	jsr JsonParser.setAsciiMode
.endm


; @access public
; @param word jsonObjectMemoryLocation
; @return void
setJsonObject .macro jsonObjectMemoryLocation
	ldx #<\jsonObjectMemoryLocation
	ldy #>\jsonObjectMemoryLocation
	jsr JsonParser.setJsonObject
.endm


; @access public
; @param word memoryLocationPointer
; @param word memoryLocation
; @return void
setJsonOutputMemoryLocation .macro memoryLocationPointer, memoryLocation
	ldx #<\memoryLocationPointer
	ldy #>\memoryLocationPointer
	jsr JsonParser.setJsonOutputMemoryLocationPointer

	ldx #<\memoryLocation
	ldy #>\memoryLocation
	jsr JsonParser.setJsonOutputMemoryLocation
.endm


; @access public
; @return void
setPetsciiMode .macro
	jsr JsonParser.setPetsciiMode
.endm


; @access public
; @return void
setStreamedJsonObject .macro
	jsr JsonParser.prepareParser
.endm


; @access public
; @param word outputMemoryPointer
; @param byte size = 0
; @return void
storeJsonKey .macro outputMemoryPointer, size = 0
	ldx #<\outputMemoryPointer
	ldy #>\outputMemoryPointer
	jsr JsonParser.storeJsonKeyFromBuffer
	.if \size
		ldx #<\size
		ldy #>\size
		jsr JsonParser.moveStoreJsonKeyPointerLocation
	.fi
.endm


; Stores the length of the key in a byte under the given location
; 
; @access public
; @param word outputMemoryPointer
; @param byte size = 0
; @return void
storeJsonKeyLength .macro outputMemoryPointer, size = 0
	ldx #<\outputMemoryPointer
	ldy #>\outputMemoryPointer
	jsr JsonParser.storeJsonKeyLength
	.if \size
		ldx #<\size
		ldy #>\size
		jsr JsonParser.moveStoreJsonKeyLengthPointerLocation
	.fi
.endm


; @access public
; @param word outputMemoryPointer
; @param byte size = 0
; @return void
storeJsonValue .macro outputMemoryPointer, size = 0
	ldx #<\outputMemoryPointer
	ldy #>\outputMemoryPointer
	jsr JsonParser.storeJsonValue
	.if \size
		ldx #<\size
		ldy #>\size
		jsr JsonParser.moveStoreJsonValuePointerLocation
	.fi
.endm


; Stores the length of the value in a word under the given location
;  
; @access public
; @param word outputMemoryPointer
; @param byte size = 0
; @return void
storeJsonValueLength .macro outputMemoryPointer, size = 0
	ldx #<\outputMemoryPointer
	ldy #>\outputMemoryPointer
	jsr JsonParser.storeJsonValueLength
	.if \size
		ldx #<\size
		ldy #>\size
		jsr JsonParser.moveStoreJsonValueLengthPointerLocation
	.fi
.endm
