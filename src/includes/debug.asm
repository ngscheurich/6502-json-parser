﻿
; @access public
; @return void
trackGetByte
	lda JsonParser.getByte+1
	sta $70
	lda JsonParser.getByte+2
	sta $71
	lda JsonParser.currentNestingLevel
	sta $75
rts
