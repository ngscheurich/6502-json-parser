﻿
.weak

; @access private
; @return A - value
getByte
	lda $1234
rts


; @access private
; @affects getByte+1
; @affects getByte+2
; @return void
walkForward
	inc getByte+1
	bne +
		inc getByte+2
+
rts


.endweak
