﻿# Changelog

## [1.0.1] - 2019-11-12
### Added
- Test JSON value lengths for streamed JSON data method
- Update c64unit to v0.71

## [1.0] - 2018-08-24
### Changed
- Update library to remove deprecated encoding in 64tass v1.53.1515
- Update c64unit to v0.65

### Fixed
- Test issue detected in 64tass v1.53.1515 for extra commas


## [0.9] - 2017-11-28
### Changed
- Update README with c64unit info

### Fixed
- External link to c64unit test framework in README


## [0.8] - 2017-11-21
### Added
- Additional streamed object test

### Changed
- Reorganization of streamed tests


## [0.7] - 2017-11-19
### Added
- `setJsonOutputMemoryLocation` method for handling repeated JSON parsing

### Fixed
- Installation include path in README


## [0.6] - 2017-09-11
### Added
- Introduce stream parsing mode
- Documentation and license

### Changed
- Move test data to the separate folder
- Update test example by moving `setStreamedJsonObject` before reading from stream


## [0.5] - 2018-08-30
### Added
- Introduce `storeJsonKeyLength` method
- Introduce `storeJsonValueLength` method
- Ability for parser to work with keys containing escaped quote characters

### Changed
- Exchange dynamically set of pointers to fixed table locations for the whole test suite


## [0.4] - 2017-08-22
### Added
- Define max length of JSON key, add tests for exceed the length of keys
- Install scripts
- ASCII to PETSCII mode functionality
- c64unit dependency script for *nix

### Removed
- Buffer for values and store data directly in tables
- Simplify functionality by removing `expectJsonValue` or `expectJsonArray`


## [0.3] - 2017-08-18
### Added
- Introduce `storeJsonKey` method
- Introduce `expectAnyJsonKey` method
- `jsonKeyLoaded` variable - this way, key can be stored even when entering an iterator
- Improve storing values in tables by introducing `storeJsonValue` method with pointer, and optional size of each value input
- Support for escaping characters like quotes

### Changed
- Use constants for ASCII characters

### Fixed
- Bug for array of integers detected in tests

### Removed
- Namespacing from object memory location constant


## [0.2] - 2017-08-15
### Added
- JSON object parsing with unit tests for data memory assertions


## [0.1], [init] - 2017-08-13
### Added
- Set JSON object method
- c64unit test framework testsuite


[1.0.1]: https://bitbucket.org/Commocore/6502-json-parser/branches/compare/HEAD..v1.0#diff
[1.0]: https://bitbucket.org/Commocore/6502-json-parser/branches/compare/v1.0..v0.9#diff
[0.9]: https://bitbucket.org/Commocore/6502-json-parser/branches/compare/v0.9..v0.8#diff
[0.8]: https://bitbucket.org/Commocore/6502-json-parser/branches/compare/v0.8..v0.7#diff
[0.7]: https://bitbucket.org/Commocore/6502-json-parser/branches/compare/v0.7..v0.6#diff
[0.6]: https://bitbucket.org/Commocore/6502-json-parser/branches/compare/v0.6..v0.5#diff
[0.5]: https://bitbucket.org/Commocore/6502-json-parser/branches/compare/v0.5..v0.4#diff
[0.4]: https://bitbucket.org/Commocore/6502-json-parser/branches/compare/v0.4..v0.3#diff
[0.3]: https://bitbucket.org/Commocore/6502-json-parser/branches/compare/v0.3..v0.2#diff
[0.2]: https://bitbucket.org/Commocore/6502-json-parser/branches/compare/v0.2..v0.1#diff
[0.1]: https://bitbucket.org/Commocore/6502-json-parser/branches/compare/v0.1..f85cc87eb727dd6db7e5c4bf340b9048b44b3eef#diff
[init]: https://bitbucket.org/Commocore/6502-json-parser/commits/f85cc87eb727dd6db7e5c4bf340b9048b44b3eef
